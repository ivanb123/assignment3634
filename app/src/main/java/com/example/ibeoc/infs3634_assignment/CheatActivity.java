package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends AppCompatActivity {
    private Button mShowAnswer;
    private TextView answerTV;
    private String answer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        mShowAnswer = findViewById(R.id.showAnswerBtn);
        answerTV = findViewById(R.id.cheatAnswer);
        Intent intent = getIntent();
        answer = intent.getStringExtra("Answer");

        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerTV.setText(answer);
            }
        });
    }
}
