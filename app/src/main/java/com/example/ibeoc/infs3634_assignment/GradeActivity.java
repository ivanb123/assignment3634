package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class GradeActivity extends AppCompatActivity {

    private String grade;
    private Button home;
    private Button reattempt;
    private TextView gradeTV;
    public String topic = "";
    public int topicInt;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);
        Intent intent = getIntent();
        gradeTV = findViewById(R.id.grade);
        topic = intent.getStringExtra("topic");
        home = findViewById(R.id.returnHome);
        reattempt = findViewById(R.id.reattemptQuiz);
        topicInt = Integer.parseInt(topic);
        System.out.println("The topic number is: " + topic);
        grade = intent.getStringExtra("grade");
        int gradeInt = Integer.parseInt(grade);
        gradeTV.setText(Integer.toString(gradeInt));
        DocumentReference docRef = db.collection("students").document(User.zid);



        Map<String, Object> user = new HashMap<>();

        if(topicInt == 1){
            user.put("zid", User.zid);
            user.put("module1Grade", grade);
            user.put("module1Progress", "3");
            user.put("module2Grade", User.module2Grade);
            user.put("module2Progress", User.module2Progress);
            user.put("module3Grade", User.module3Grade);
            user.put("module3Progress", User.module3Progress);

            db.collection("students").document(User.zid).set(user)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });
            }
        else if(topicInt == 2){
            user.put("zid", User.zid);
            user.put("module1Grade", User.module1Grade);
            user.put("module1Progress", User.module1Progress);
            user.put("module2Grade", grade);
            user.put("module2Progress", "3");
            user.put("module3Grade", User.module3Grade);
            user.put("module3Progress", User.module3Progress);
            db.collection("students").document(User.zid).set(user)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });
        }
        else{
            user.put("zid", User.zid);
            user.put("module1Grade", User.module1Grade);
            user.put("module1Progress", User.module1Progress);
            user.put("module2Grade", User.module2Grade);
            user.put("module2Progress", User.module2Progress);
            user.put("module3Grade", grade);
            user.put("module3Progress", "3");

            db.collection("students").document(User.zid).set(user)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });
        }

        reattempt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GradeActivity.this, QuizActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GradeActivity.this, LandingActivity.class);
                intent.putExtra("zID", User.zid);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed(){

    }
}
