package com.example.ibeoc.infs3634_assignment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class LandingActivity extends AppCompatActivity {

    TextView text1,text2,text3,text4,text5;
    Button topic1btn,topic2btn,topic3btn;
    private String zID;
    FirebaseFirestore db;
    DocumentReference docRef;
    private TextView landingCompleted;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);
        landingCompleted = findViewById(R.id.landingCompleted);
        Intent intent = getIntent();
        zID = intent.getStringExtra("zID");
        topic1btn = findViewById(R.id.topic1btn);
        topic2btn = findViewById(R.id.topic2btn);
        topic3btn = findViewById(R.id.topic3btn);
        //FirebaseApp.initializeApp(LandingActivity.this);
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("students").document(zID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    User.zid = zID;

                    if (document.exists()) {

                        User.zid = zID;
                        User.module1Progress = document.getString("module1Progress");
                        User.module2Progress = document.getString("module2Progress");
                        User.module3Progress = document.getString("module3Progress");
                        User.module1Grade = document.getString("module1Grade");
                        User.module2Grade = document.getString("module2Grade");
                        User.module3Grade = document.getString("module3Grade");
                        User.modulesCompleted = User.getModulesCompleted();
                        landingCompleted.setText("You have completed " + Integer.toString(User.getModulesCompleted()) + " modules");


                    }
                    else {

                        Map<String, Object> user = new HashMap<>();

                            user.put("module1Progress", "0");
                            user.put("module2Progress", "0");
                            user.put("zid", User.zid);
                            user.put("module3Progress", "0");
                            user.put("module1Grade", "");
                            user.put("module2Grade", "");
                            user.put("module3Grade", "");

                            db.collection("students").document(User.zid).set(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                        }
                                    });
                    }}
            }
        });




        topic1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this, Topic1.class);
                startActivity(intent);
            }
        });
        topic2btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this, Topic2.class);
                startActivity(intent);
            }
        });
        topic3btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this, Topic3.class);
                startActivity(intent);
            }
        });

    }



}

