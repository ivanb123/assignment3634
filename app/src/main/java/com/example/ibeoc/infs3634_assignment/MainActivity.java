package com.example.ibeoc.infs3634_assignment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private UNSWAPI zLogin;
    private static Context mContext;
    private EditText zID;
    private EditText zPass;
    private Button signInBtn;
    private TextView warningText;
    public static String zIDString;

    private static String currentZID;
    private String offlineZID = "z7654321";
    private String offlineZPass = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        zLogin = new UNSWAPI("http://10.0.0.214:5000/login", "INFS3634", getApplicationContext());
        //mContext = getApplicationContext();
        mContext = MainActivity.this;
        signInBtn = findViewById(R.id.signInBtn);
        zID = findViewById(R.id.zID);
        zPass = findViewById(R.id.zPass);

    //When you press the button it checks the server by instantiating an object of zlogin from the UNSWAPI class (which we have declared
    //at the top). Then because it's an asynchronous task you use the try catch block.
}
    public void signUpBtnClick(View view){
        zLogin.setzID(zID.getText().toString());
        zLogin.setzPass(zPass.getText().toString());
        zIDString = zID.getText().toString();
        if (zLogin.validateLogin()){
            try{
                if(zID.getText().toString().equals(offlineZID) && zPass.getText().toString().equals(offlineZPass)){
                    currentZID = offlineZID;
                    handleLogin(true);
                }
                else{
                Toast.makeText(mContext, "Connecting to server...", Toast.LENGTH_LONG).show();
                boolean authenticator = zLogin.tryLogin();
            }}
            catch(Exception e){
                e.printStackTrace();
            }

        }
        else{
            Toast.makeText(mContext, "Please fill all text fields properly", Toast.LENGTH_LONG).show();


        }

    }



    public static void handleLogin(boolean authenticated){
        if(authenticated){
            System.out.println("nice");
            Intent intent = new Intent(mContext, LandingActivity.class);
            intent.putExtra("zID", zIDString);
            mContext.startActivity(intent);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        }
        else{
            System.out.println("not nice");
            Toast.makeText(mContext, "Incorrect Login Details. Try Again.", Toast.LENGTH_LONG).show();
        }
    }
}
