package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private Button mTrueButton;
    private Button mFalseButton;
    private Button mNextButton;
    private Button mCheatButton;
    private int grade;
    private boolean cheatButtonClicked;
    private TextView mQuestionTextView;
    public String topic = "";

    private Question[] mQuestionBank = new Question[]{
            new Question(R.string.t1q1, false),
            new Question(R.string.t1q2, false),
            new Question(R.string.t1q3, true),
            new Question(R.string.t1q4, true),
            new Question(R.string.t1q5, false),
            new Question(R.string.t1q6, true),
    };

    private int mCurrentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        int grade = 0;
        Intent intent = getIntent();
        topic = intent.getStringExtra("topic");
        int topicInt = Integer.parseInt(topic);
        if(topicInt == 2){
            mQuestionBank = new Question[]
                    {       new Question(R.string.t2q1, true),
                            new Question(R.string.t2q2, false),
                            new Question(R.string.t2q3, true),
                            new Question(R.string.t2q4, false),
                            new Question(R.string.t2q5, false),
                            new Question(R.string.t2q6, true),};
        }
        else if(topicInt == 3){
            mQuestionBank = new Question[]
                    {       new Question(R.string.t3q1, false),
                            new Question(R.string.t3q2, true),
                            new Question(R.string.t3q3, false),
                            new Question(R.string.t3q4, true),
                            new Question(R.string.t3q5, false),
                            new Question(R.string.t3q6, false),};
        }

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mCheatButton = findViewById(R.id.cheat_button);
        mTrueButton = (Button) findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {

            @Override

            //In makeText(), QuizActivity is passed as the Context argument; this refers to the anonymous class View.OnClickListener
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizActivity.this, CheatActivity.class);
                intent.putExtra("Answer", String.valueOf(mQuestionBank[mCurrentIndex].isAnswerTrue()));
                cheatButtonClicked = true;
                startActivity(intent);
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("The topic number is: " + topic);

                if(mCurrentIndex != mQuestionBank.length - 1){
                mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                updateQuestion();
                }
                else{
                result();
                }
            }
        });
        updateQuestion();
        cheatButtonClicked = false;

    }

    private void updateQuestion(){
        int  question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
    }
    private void checkAnswer(boolean userPressedTrue){
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();

        int messageResId = 0;
        if(cheatButtonClicked){
            messageResId = R.string.cheater_toast;
            cheatButtonClicked = false;
        }

        else if (userPressedTrue == answerIsTrue){
            messageResId = R.string.correct_Toast;
            grade ++;
        }
        else {
            messageResId = R.string.inCorrect_Toast;

        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
        cheatButtonClicked = false;

    }

    void result(){
            Intent intent = new Intent(QuizActivity.this,
                    GradeActivity.class);
            intent.putExtra("grade", Integer.toString(grade));
            intent.putExtra("topic", topic);
            startActivity(intent);


    }
}

