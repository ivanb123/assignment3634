package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ReadingsActivity extends AppCompatActivity {
    private TextView readingsTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readings);
        Intent intent = getIntent();
        readingsTV = findViewById(R.id.readings);
        String topic = intent.getStringExtra("topic");
        int topicInt = Integer.parseInt(topic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(topicInt == 1){
            setTitle(R.string.activitiesLabel);
            readingsTV.setText(R.string.readings1);

        }
        if(topicInt == 2){
            setTitle(R.string.apiLabel);
            readingsTV.setText(R.string.readings2);
        }
        if(topicInt == 3){
            setTitle(R.string.databaseLabel);
            readingsTV.setText(R.string.readings3);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
    }
}
