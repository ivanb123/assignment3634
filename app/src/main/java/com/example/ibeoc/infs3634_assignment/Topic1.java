package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Topic1 extends AppCompatActivity {

    TextView text11,text12;
    private TextView completed;
    Button you1btn;
    Button readings1btn;
    Button quiz1btn;
    public static final String topic = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic1);
        completed = findViewById(R.id.completed);
        int TopicCompleted = 0;
        readings1btn = findViewById(R.id.readings1btn);
        quiz1btn = findViewById(R.id.quiz1btn);
        if (User.module1Progress != null) {
            TopicCompleted = Integer.parseInt(User.module1Progress);
        }
        if(User.module1Grade!= null){
        System.out.println("TopicCompleted = " + TopicCompleted);
        if (TopicCompleted == 3 && User.module1Grade != ""){
            int userGrade = Integer.parseInt(User.module1Grade);

            completed.setText("You have completed this module with a grade of: " + userGrade);
        }
        else{
            completed.setText("You have not completed this module");
        }}
        else{
            completed.setText("You have not completed this module");
        }

        you1btn = findViewById(R.id.you1btn);

        you1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Topic1.this, youtube1.class);

                startActivity(intent);
            }
        });

        readings1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic1.this, ReadingsActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
        quiz1btn.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic1.this, QuizActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
    }
}
