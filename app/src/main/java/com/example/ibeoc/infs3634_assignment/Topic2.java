package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Topic2 extends AppCompatActivity {

    TextView text11,text12;
    private TextView completed2;
    Button you2btn;
    Button readings2btn;
    Button quiz2btn;
    public static final String topic = "2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic2);
        completed2 = findViewById(R.id.completed2);
        int TopicCompleted = 0;
        readings2btn = findViewById(R.id.readings2btn);
        quiz2btn = findViewById(R.id.quiz2btn);
        if (User.module2Progress != null) {
            TopicCompleted = Integer.parseInt(User.module2Progress);
        }
        if(User.module2Grade!= null){
            System.out.println("TopicCompleted = " + TopicCompleted);
            if (TopicCompleted == 3 && User.module2Grade != ""){
                int userGrade = Integer.parseInt(User.module2Grade);

                completed2.setText("You have completed this module with a grade of: " + userGrade);
            } else{
                completed2.setText("You have not completed this module");
            }}
        else{
            completed2.setText("You have not completed this module");
        }

        you2btn = findViewById(R.id.you2btn);

        you2btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Topic2.this, youtube2.class);

                startActivity(intent);
            }
        });

        readings2btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic2.this, ReadingsActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
        quiz2btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic2.this, QuizActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
    }
}

