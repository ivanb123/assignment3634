package com.example.ibeoc.infs3634_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Topic3 extends AppCompatActivity {

    TextView text11,text12;
    private TextView completed3;
    Button you3btn;
    Button readings3btn;
    Button quiz3btn;
    public static final String topic = "3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic3);
        completed3 = findViewById(R.id.completed3);
        int TopicCompleted = 0;
        readings3btn = findViewById(R.id.readings3btn);
        quiz3btn = findViewById(R.id.quiz3btn);
        if (User.module3Progress != null) {
            TopicCompleted = Integer.parseInt(User.module3Progress);
        }
        if(User.module3Grade!= null){
            System.out.println("TopicCompleted = " + TopicCompleted);
            if (TopicCompleted == 3 && User.module3Grade != ""){
                int userGrade = Integer.parseInt(User.module3Grade);

                completed3.setText("You have completed this module with a grade of: " + userGrade);
            } else{
                completed3.setText("You have not completed this module");
            }}
        else{
            completed3.setText("You have not completed this module");}

        you3btn = findViewById(R.id.you3btn);

        you3btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Topic3.this, youtube3.class);

                startActivity(intent);
            }
        });

        readings3btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic3.this, ReadingsActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
        quiz3btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Topic3.this, QuizActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });

}}

