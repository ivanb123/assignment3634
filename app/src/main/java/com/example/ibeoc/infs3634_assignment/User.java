package com.example.ibeoc.infs3634_assignment;

public class User {

    public static String zid;
    public static String module1Progress = "0";
    public static String module2Progress = "0";
    public static String module3Progress = "0";
    public static String module1Grade = "";
    public static String module2Grade = "";
    public static String module3Grade = "";
    public static int modulesCompleted;

    public static int getModulesCompleted() {
        int completed = 0;
        if(module1Progress != null && module2Progress != null && module3Progress!= null){
            if (Integer.parseInt(module1Progress) == 3 && (module1Grade != "" || module1Grade != null)) {
                completed++;
            }
            if (Integer.parseInt(module2Progress) == 3 &&(module2Grade != "" || module2Grade != null)) {
                completed++;
            }
            if (Integer.parseInt(module3Progress) == 3 && (module3Grade != "" || module3Grade != null)) {
                completed++;

            }}
        return completed;
    }


}